import { DataTypes } from "sequelize";
import { sequelize } from "../database/database.js";
import { Categoria } from "./Categoria.js";
import { Usuario } from "./Usuario.js";

export const Producto = sequelize.define(
    'productos',
    {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        nombre: {
            type: DataTypes.STRING,
            allowNull: false,
            comment: 'Nombre del Producto',
        },
        precio_unitario: {
            type: DataTypes.DECIMAL(10,2),
            allowNull: false,
            comment: 'Precio Unitario del Producto',
        },
        estado: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            comment: 'Estado del Producto',
        },
    },
    {
        timestamps: false,
    },
);


Categoria.hasMany(Producto,{
    foreignKey: 'categoria_id',
    sourceKey: 'id',
});


Producto.belongsTo(Categoria, {
    foreignKey: 'categoria_id',
    targetKey: 'id',
});
