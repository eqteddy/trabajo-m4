import { DataTypes } from "sequelize";
import { sequelize } from "../database/database.js";
import { Producto } from "./Producto.js";
import { Categoria } from "./Categoria.js";

export const Usuario = sequelize.define(
    'usuarios',
    {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        nombre: {
            type: DataTypes.STRING,
            allowNull: false,
            comment: 'Nombre del usuario',
        },
        correo: {
            type: DataTypes.STRING,
            allowNull: false,
            comment: 'Correo del Usuario',
        },
        contrasena: {
            type: DataTypes.STRING,
            allowNull: false,
            comment: 'Contraseña del Usuario',
        },
        estado: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            comment: 'Estado del Usuario',
        },             
    },
    {
        timestamps: false,
    },
);

Usuario.hasMany(Categoria, {
    foreignKey: 'usuario_id',
    sourceKey: 'id',
});

Categoria.belongsTo(Usuario, {
    foreignKey: 'usuario_id',
    targetKey: 'id',
});

Usuario.hasMany(Producto,{
    foreignKey: 'usuario_id',
    sourceKey: 'id',
});

Producto.belongsTo(Usuario, {
    foreignKey: 'usuario_id',
    targetKey: 'id',
})