import { Sequelize } from "sequelize";
import 'dotenv/config'

const {DB_NAME,DB_USER,DB_PASSWORD,DB_HOST} = process.env;
export const sequelize = new Sequelize(
    DB_NAME, //dbname
    DB_USER, //username
    DB_PASSWORD, //password
    {
        host: DB_HOST,
        dialect: 'postgres',
    },
);