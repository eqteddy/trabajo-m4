import { Usuario } from '../models/Usuario.js'
import jwt from 'jsonwebtoken'
import 'dotenv/config'


export async function getLogin ( req, res) {
    const { correo, contrasena } = req.body;
    const { TOKEN_SECRET } = process.env;

    console.log(TOKEN_SECRET);
    try {
        const usuario = await Usuario.findOne({
            where: {
                correo,
                contrasena
            }
        });
        if(usuario) {
            jwt.sign({ usuario },TOKEN_SECRET,(err, token)=>{
                res.json(token);
            })
              
        }
        else {
            res.sendStatus(403);
        }
        
    } catch (error) {
        res.status(500).json({
            message: error,
        });
    }
}

export function verifyToken (req, res, next) {
    const headerBeader = req.headers['authorization'];
    const { TOKEN_SECRET } = process.env;
    if(typeof headerBeader !== 'undefined') {
        const token = headerBeader.split(' ')[1];
        jwt.verify(token,TOKEN_SECRET,(err, usuario)=>{
            if(err){
                res.sendStatus(403);
            }
            else{
                req.usuario = usuario;
                next();
            }
        })
    }
    else res.sendStatus(403);

}