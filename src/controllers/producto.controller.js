import { Usuario } from '../models/Usuario.js'
import { Categoria } from '../models/Categoria.js'
import { Producto } from '../models/Producto.js'

export async function getProductos ( req, res) {
    try {
        const productos = await Producto.findAll({
            attributes: ['id','nombre','precio_unitario','estado','categoria_id','usuario_id'],
        });
        res.json(productos);
    } catch (error) {
        res.status(500).json({
            message: error,
        });
    }
}

export async function createProducto ( req, res) {
    try {
        const { nombre,precio_unitario,estado,categoria_id,usuario_id } = req.body;

        console.log(req.body);
        const newProducto = await Producto.create({
            nombre,
            precio_unitario,
            estado,
            categoria_id,
            usuario_id,
        },{
            fields: ['nombre','precio_unitario','estado','categoria_id','usuario_id'],
        });
        return res.json(newProducto);
    } catch (error) {
        res.status(500).json({
            message: error,
        }); 
    }
}

export async function getProducto ( req, res) {
    const { id } = req.params;
    
    try {
        const producto = await Producto.findByPk(id,{
            attributes: ['nombre','precio_unitario','estado','categoria_id','usuario_id'],
        });
        res.json(producto);
    } catch (error) {
        res.status(500).json({
            message: error,
        });
    }
}


export async function updateProducto ( req, res) {
    const { id } = req.params;
    const { nombre,precio_unitario,estado,categoria_id,usuario_id } = req.body;
    try {
        const producto = await Producto.findByPk(id);
        producto.nombre = nombre;
        producto.precio_unitario = precio_unitario;
        producto.estado = estado;
        producto.categoria_id = categoria_id;
        producto.usuario_id = usuario_id;

        await producto.save();
        res.json(producto);
    } catch (error) {
        res.status(500).json({
            message: error,
        });
    }
}

export async function deleteProducto ( req, res) {
    const { id } = req.params;
    
    try {
        await Producto.destroy({
            where: { id },
        });
        res.sendStatus(204);
    } catch (error) {
        res.status(500).json({
            message: error,
        });
    }
}
