import { Usuario } from '../models/Usuario.js'
import { Categoria } from '../models/Categoria.js'
import { Producto } from '../models/Producto.js'
import logger from '../logs/logger.js'



export async function getUsuarioCatProd ( req, res) {
    const { id } = req.params;
    
    try {
        const usuario = await Usuario.findByPk(id,{
            attributes: ['id','nombre','correo','estado'],
        });
        const categorias = await Categoria.findAll({
            where: {
                usuario_id: id,
            }
        });
        const productos = await Producto.findAll({
            where: {
                usuario_id: id,
            }
        });
        res.json({usuario, categorias,productos
        });
    } catch (error) {
        res.status(500).json({
            message: error,
        });
    }
}



export async function getUsuarios ( req, res) {
    try {
        const usuarios = await Usuario.findAll({
            attributes: ['id','nombre','correo','estado'],
        });
        res.json(usuarios);
    } catch (error) {
        logger.error(error);
        res.status(500).json({
            message: error,
        });
        
    }
}

export async function createUsuario ( req, res) {
    try {
        const { nombre, correo, contrasena, estado } = req.body;

        console.log(req.body);
        const newUsuario = await Usuario.create({
            nombre,
            correo,
            contrasena,
            estado,
        },{
            fields: ['nombre','correo','contrasena','estado'],
        });
        return res.json(newUsuario);
    } catch (error) {
        res.status(500).json({
            message: error,
        }); 
    }
}

export async function getUsuario ( req, res) {
    const { id } = req.params;
    
    try {
        const usuario = await Usuario.findByPk(id,{
            attributes: ['id','nombre','correo','estado'],
        });
        res.json(usuario);
    } catch (error) {
        res.status(500).json({
            message: error,
        });
    }
}


export async function updateUsuario ( req, res) {
    const { id } = req.params;
    const { nombre, correo, contrasena, estado } = req.body;
    try {
        const usuario = await Usuario.findByPk(id);
        usuario.nombre = nombre;
        usuario.correo = correo;
        usuario.contrasena = contrasena;
        usuario.estado = estado;

        await usuario.save();
        res.json(usuario);
    } catch (error) {
        res.status(500).json({
            message: error,
        });
    }
}

export async function deleteUsuario ( req, res) {
    const { id } = req.params;
    
    try {
        await Producto.destroy({
            where: { usuario_id: id }
        })
        await Categoria.destroy({
            where: { usuario_id: id }
        })
        await Usuario.destroy({
            where: { id },
        });
        res.sendStatus(204);
    } catch (error) {
        res.status(500).json({
            message: error,
        });
    }
}
