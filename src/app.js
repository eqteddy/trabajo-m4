import express from "express";
import morgan from "morgan";
import { verifyToken } from "./controllers/login.controller.js";


const app = express();

//Import routes
import usuarioRoutes from './routes/usuarios.routes.js';
import categoriaRoutes from './routes/categorias.routes.js';
import productoRoutes from './routes/productos.routes.js';
import loginRoutes from './routes/login.routes.js';

import { json } from "sequelize";

//Middlewares
app.use(morgan('dev'));
app.use(express.json());
//Routes
app.use('/api/login',loginRoutes);
app.use('/api/usuarios',verifyToken,usuarioRoutes);
app.use('/api/categorias',verifyToken,categoriaRoutes);
app.use('/api/productos',verifyToken,productoRoutes);


export default app;