import { Router } from "express";
import { getCategorias,createCategoria,getCategoria,deleteCategoria, updateCategoria } from "../controllers/categoria.controller.js";

const router = new Router();

//Rutas

router.get('/',getCategorias);

router.post('/',createCategoria);

router.get('/:id',getCategoria);

router.put('/:id',updateCategoria);

router.delete('/:id',deleteCategoria);



export default router;