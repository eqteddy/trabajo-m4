import { Router } from "express";
import { createUsuario, deleteUsuario, getUsuario, getUsuarioCatProd, getUsuarios, updateUsuario } from "../controllers/usuario.controller.js";
import { verifyToken } from '../controllers/login.controller.js'

const router = new Router();

//Rutas
router.get('/CatProd/:id',verifyToken,getUsuarioCatProd);

router.get('/',verifyToken,getUsuarios);

router.post('/',createUsuario);

router.get('/:id',getUsuario);

router.put('/:id',updateUsuario);

router.delete('/:id',deleteUsuario);


export default router;