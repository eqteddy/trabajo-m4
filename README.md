# DIPLOMADO FULLSTACK DEVELOPER - MÓDULO IV

## EXAMEN FINAL 

## Instalación
### Base de datos

1. Se debe crear la Base de Datos **productos_db** (postgres)
2. Se configurar el archivo .env:

```sql
PORT=3002 //listening port for web server
TOKEN_SECRET= //secret for Token jwt
DB_NAME= //Name Database 
DB_USER= //User Database
DB_PASSWORD= //Password for user database
DB_HOST= //ip or fqdn for host database
```
# Aplicación

1. se debe clonar la aplicación
```cli
git clone https://gitlab.com/eqteddy/trabajo-m4.git
```
2. Arranque la aplicación
```npm
npm run dev
```

# Ejecución
1. Se crea el usuario admin mediante el siguiente script
```sql
insert into usuarios (nombre,correo,contrasena,estado)
values ('admin','admin@admin.com','secret123',true)

insert into categorias (nombre,usuario_id) 
values 
('Celulares','1')
('Televisores','1')

insert into productos (nombre, precio_unitario,estado, categoria_id, usuario_id)
values
('Samsung Flip 5','12000.0',true,1,1) 
('Samsung S23','10000.0',true,1,1)
('Samsung Neo QLED 55','12000.0',true,2,1)
('Samsung CRYSTAL 65','7000.0',true,2,1)

```
2. Con el usuario admin se podra crear obtener el token para agregar mas usuarios, categorias y productos. Los usuarios creados tambien podran hacer el CRUD de las tablas usuarios, categorias y productos.

3. Los rutas de las apis son:
- http://localhost:3002/api/login 
- http://localhost:3002/api/usuarios
- http://localhost:3002/api/categorias
- http://localhost:3002/api/productos

4. Ejemplo de consulta /api/usuarios/catprod/1

```json
{
	"usuario": {
		"id": 1,
		"nombre": "admin",
		"correo": "admin@admin.com",
		"estado": true
	},
	"categorias": [
		{
			"id": 1,
			"nombre": "Celulares",
			"usuario_id": 1
		},
		{
			"id": 2,
			"nombre": "Televisores",
			"usuario_id": 1
		}
	],
	"productos": [
		{
			"id": 1,
			"nombre": "Samsung Flip 5",
			"precio_unitario": "12000.00",
			"estado": true,
			"categoria_id": 1,
			"usuario_id": 1
		},
		{
			"id": 2,
			"nombre": "Samsung S23",
			"precio_unitario": "10000.00",
			"estado": true,
			"categoria_id": 1,
			"usuario_id": 1
		},
		{
			"id": 3,
			"nombre": "Samsung Neo QLED 55",
			"precio_unitario": "12000.00",
			"estado": true,
			"categoria_id": 2,
			"usuario_id": 1
		},
		{
			"id": 4,
			"nombre": "Samsung CRYSTAL 65",
			"precio_unitario": "7000.00",
			"estado": true,
			"categoria_id": 2,
			"usuario_id": 1
		}
	]
}
```
